def info_decorator(func):
    def wrapper(*args, reverse=False):
        print(f"Function {func.__name__} was called")
        return func(*args)
    return wrapper


def mul_decorator(multiplier=1):
    def decor(func):
        def wrapper(*args, reverse=False):
            lst_args = list(args)
            lst_args[-1] *= multiplier
            return func(*lst_args)
        return wrapper
    return decor
