from decorators import decor


@decor.mul_decorator(1)
@decor.info_decorator
def sum(*args, reverse=False):
    res = 0
    if reverse:
        args = list(args).reverse()
    for arg in args:
        res += arg
    return res


@decor.mul_decorator(1)
@decor.info_decorator
def sub(*args, reverse= False):
    res = 0
    if reverse:
        args = list(args).reverse()
    for arg in args:
        res -= arg
    return res


@decor.mul_decorator(1)
@decor.info_decorator
def mul(*args, reverse=False):
    res = 0
    if reverse:
        args = list(args).reverse()
    for arg in args:
        res *= arg
    return res


@decor.mul_decorator(1)
@decor.info_decorator
def div(*args, reverse=False):
    res = 0
    if reverse:
        args = list(args).reverse()
    for arg in args:
        res /= arg
    return res

