from project.database_structure import Customer, Rack, Room, session


def get_all_rooms():
    """Get all rooms."""
    rooms = session.query(Room).all()
    result = []
    for room in rooms:
        tmp = {
            'id': room.id,
            'name': room.name
        }
        result.append(tmp)
    return result


def get_all_racks():
    """Get all racks."""
    racks = session.query(Rack).all()
    result = []
    for rack in racks:
        tmp = {
            'id': rack.id,
            'name': rack.name,
            'size': rack.size,
            'state': rack.state,
            'customer': rack.customer,
            'room_id': rack.room_id
        }
        result.append(tmp)
    return result


def get_all_customers():
    """Get all customers."""
    customers = session.query(Customer).all()
    result = []
    for customer in customers:
        tmp = {
            'id': customer.id,
            'name': customer.name
        }
        result.append(tmp)
    return result


def get_occupied_racks():
    """Get occupied racks."""
    racks = session.execute("SELECT rack.id, rack.name, customer.name, room.name FROM rack\
                            JOIN customer ON customer.id = rack.customer\
                            JOIN room ON room.id = rack.room_id\
                            WHERE rack.state = 'occupied'")
    racks = racks.fetchall()
    result = []
    for rack in racks:
        tmp = {
            'rack id': rack[0],
            'rack name': rack[1],
            'customers name': rack[2],
            'room name': rack[3],
        }
        result.append(tmp)
    return result


def get_max_rack():
    """Get max rack in room."""
    racks = session.execute("SELECT DISTINCT ON (room.id) room.id, rack.id, MAX(rack.size) FROM room\
                            JOIN rack ON room.id = rack.room_id\
                            GROUP BY room.id, rack.id")
    racks = racks.fetchall()
    result = []
    for rack in racks:
        tmp = {
            'room id': rack[0],
            'rack id': rack[1],
            'Max size': rack[2],
        }
        result.append(tmp)
    return result


def get_customers_in_room():
    rooms = session.execute("SELECT room.id, room.name, rack.customer from room\
                                JOIN rack ON room.id = rack.room_id\
                                    WHERE rack.state = 'occupied'")
    rooms = rooms.fetchall()
    result = {}
    for room in rooms:
        if rooms[0] in result.keys():
            result[room[0]]['customer id'].append(room[2])
        else:
            result[room[0]] = {'room name': room[1], 'customer list': [room[2]]}
    return result