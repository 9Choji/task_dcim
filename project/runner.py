"""This module runs application."""
from project.app import app


def main():
    """App run."""
    app.run('127.0.0.1', port=5005)
