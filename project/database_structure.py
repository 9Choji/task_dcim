from sqlalchemy import Column, ForeignKey, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database
Base = declarative_base()
#SQL_STRING = "postgresql://postgres:fghtkm98@localhost:5432/test2"
#engine = create_engine(SQL_STRING)
engine = create_engine("postgres://postgres:password@localhost/test_db")
if not database_exists(engine.url):
    create_database(engine.url)
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()


class Room(Base):
    __tablename__ = "room"

    id = Column(Integer, primary_key=True)
    name = Column(String)

    children = relationship("Rack")


class Rack(Base):
    __tablename__ = "rack"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    size = Column(Integer)
    state = Column(String)
    customer = Column(Integer, ForeignKey("customer.id"))
    room_id = Column(Integer, ForeignKey("room.id"))


class Customer(Base):
    __tablename__ = "customer"

    id = Column(Integer, primary_key=True)
    name = Column(String)

    children = relationship("Rack")


Base.metadata.create_all(engine)
