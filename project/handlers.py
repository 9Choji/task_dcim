"""This file contains handlers for api."""
from project.app import app
import project.database_manager as dm
from flask import render_template, jsonify, request
from decorators import funcs as df


@app.route('/')
def index():
    """Return index page."""
    return render_template('info.html')


@app.route('/rooms', methods=['GET'])
def showRooms():
    rooms = dm.get_all_rooms()
    return jsonify({'rooms': rooms})


@app.route('/racks', methods=['GET'])
def showRacks():
    racks = dm.get_all_racks()
    return jsonify({'racks': racks})


@app.route('/customers', methods=['GET'])
def showCustomers():
    customers = dm.get_all_customers()
    return jsonify({'customers': customers})


@app.route('/occupied', methods=['GET'])
def showOccupiedRaks():
    racks = dm.get_occupied_racks()

    return jsonify({'racks': racks})


@app.route('/max_rack', methods=['GET'])
def showMaxRaks():
    racks = dm.get_max_rack()
    return jsonify({'racks': racks})


@app.route('/customers_in_room', methods=['GET'])
def showCustomersInRoom():
    racks = dm.get_customers_in_room()
    return jsonify({'racks id': racks})


@app.route('/funcs', methods=['GET', 'POST'])
def funcs():
    funcs_dict = {
        'sum': df.sum,
        'sub': df.sub,
        'mul': df.mul,
        'div': df.div,
    }
    message = ''
    if request.method == 'POST':
        func = request.form.get('func')  # запрос к данным формы
        params = request.form.get('params')
        params_list = params.split(',')
        if params_list[-1] == "True" or params_list[-1] == "False":
            res = funcs_dict[func](*tuple(map(float, params_list[:-1])), reverse=(params_list[-1] == "True"))
        else:
            res = funcs_dict[func](*tuple(map(float, params_list)))
        message = func + " of " + params + " = " + str(res)
    return render_template('form_for_decorator.html', message=message)
