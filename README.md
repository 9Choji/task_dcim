# task_dcim

## Установка виртуального окружения:
```
pip install virtualenv
virtualenv venv
venv\Scripts\activate.bat
```
## Установка необходимых пакетов:
```
pip install -r requirements.txt
```
## Заполнение базы:
```
python .\migration\manage.py version_control
python .\migration\manage.py upgrade
```
## Запуск программы:
```
python run.py
```
