#!/usr/bin/env python
from migrate.versioning.shell import main
import sys
from os.path import dirname, join, abspath

sys.path.insert(0, abspath(join(dirname(__file__), '..')))

from project.database_structure import SQL_STRING

if __name__ == '__main__':
    main(url=SQL_STRING, repository='migration', debug='False')