from sqlalchemy import *
from migrate import *
from random import randint

from project.database_structure import Room, Rack, Customer

MAX_ROWS = 100


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata

    # session.add(Room(name="TEST1"))
    # session.commit()
    # or

    for num in range(1, MAX_ROWS):
        Room.__table__.insert().values(name=f"Room{num}").execute()
        Customer.__table__.insert().values(name=f"Клиент-{num}").execute()

    for num in range(1, MAX_ROWS):
        size = 42
        state = "occupied"
        if num % 2 == 0:
            size = 21
        if num % 3 == 0:
            state = "free"

        Rack.__table__.insert().values(
            name=f"Rack-{num}",
            size=size,
            state=state,
            customer=randint(1, MAX_ROWS-1),
            room_id=randint(1, MAX_ROWS-1)
        ).execute()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    Room.__table__.delete().execute()
    Customer.__table__.delete().execute()
    Rack.__table__.delete().execute()
